import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv2D, Flatten, Dropout, MaxPooling2D
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from sklearn.metrics import classification_report, confusion_matrix
from PIL import Image
from pathlib import Path
import os
import matplotlib.pyplot as plt
from torchvision.datasets import ImageFolder
import torchvision.transforms as T
import visualkeras

# show the version of tensorflow package
print("Done with library declaration, Current version of Tensorflow is: ", tf.__version__)
data_dir = Path('D:/sources/testFolder/Garbage/original_images')

transformer = T.Compose([T.Resize((32, 32)), T.ToTensor()])
dataset = ImageFolder(data_dir, transform=transformer)

# display class names
print(dataset.classes)
# display class distribution
fig = plt.figure()
ax = fig.add_axes([0, 0, 1, 1])
counts = [393, 491, 400, 584, 472, 127]
ax.bar(dataset.classes, counts)
plt.title('Class Distribution')
plt.show()

PATH_TRAIN = r"Garbage\original_images"
PATH_TEST = r"Garbage\processed_images"
class_names = ['cardboard', 'glass', 'metal', 'paper', 'plastic', 'trash']

imagepath_cardboard = r"Garbage\original_images\cardboard"
graypath_cardboard = r"Garbage\processed_images\cardboard"
File_listing = os.listdir(imagepath_cardboard)
for file in File_listing:
    im = Image.open(imagepath_cardboard + '\\' + file)
    img = im.resize((32, 32))
    gray = img.convert('L')
    gray.save(graypath_cardboard + '\\' + file, "JPEG")

imagepath_glass = r"Garbage\original_images\glass"
graypath_glass = r"Garbage\processed_images\glass"
File_listing = os.listdir(imagepath_glass)
for file in File_listing:
    im = Image.open(imagepath_glass + '\\' + file)
    img = im.resize((32, 32))
    gray = img.convert('L')
    gray.save(graypath_glass + '\\' + file, "JPEG")

imagepath_metal = r"Garbage\original_images\metal"
graypath_metal = r"Garbage\processed_images\metal"
File_listing = os.listdir(imagepath_metal)
for file in File_listing:
    im = Image.open(imagepath_metal + '\\' + file)
    img = im.resize((32, 32))
    gray = img.convert('L')
    gray.save(graypath_metal + '\\' + file, "JPEG")

imagepath_paper = r"Garbage\original_images\paper"
graypath_paper = r"Garbage\processed_images\paper"
File_listing = os.listdir(imagepath_paper)
for file in File_listing:
    im = Image.open(imagepath_paper + '\\' + file)
    img = im.resize((32, 32))
    gray = img.convert('L')
    gray.save(graypath_paper + '\\' + file, "JPEG")

imagepath_plastic = r"Garbage\original_images\plastic"
graypath_plastic = r"Garbage\processed_images\plastic"
File_listing = os.listdir(imagepath_plastic)
for file in File_listing:
    im = Image.open(imagepath_plastic + '\\' + file)
    img = im.resize((32, 32))
    gray = img.convert('L')
    gray.save(graypath_plastic + '\\' + file, "JPEG")

imagepath_trash = r"Garbage\original_images\trash"
graypath_trash = r"Garbage\processed_images\trash"
File_listing = os.listdir(imagepath_trash)
for file in File_listing:
    im = Image.open(imagepath_trash + '\\' + file)
    img = im.resize((32, 32))
    gray = img.convert('L')
    gray.save(graypath_trash + '\\' + file, "JPEG")

train_dir = os.path.join(PATH_TRAIN)
test_dir = os.path.join(PATH_TEST)
imagepath_cardboard_dir = os.path.join(imagepath_cardboard)
imagepath_glass_dir = os.path.join(imagepath_glass)
imagepath_metal_dir = os.path.join(imagepath_metal)
imagepath_paper_dir = os.path.join(imagepath_paper)
imagepath_plastic_dir = os.path.join(imagepath_plastic)
imagepath_trash_dir = os.path.join(imagepath_trash)
len(os.listdir(PATH_TRAIN))
# print(len(os.listdir(PATH_TRAIN))) check return quantity of specified path

IMG_HEIGHT = 32
IMG_WIDTH = 32

# Image data generator for training and testing
image_gen = ImageDataGenerator(rescale=1. / 255)
train_data_gen = image_gen.flow_from_directory(
    directory=train_dir,
    shuffle=True,
    target_size=(IMG_HEIGHT, IMG_WIDTH),
    class_mode='categorical')
test_data_gen = image_gen.flow_from_directory(
    directory=test_dir,
    shuffle=True,
    target_size=(IMG_HEIGHT, IMG_WIDTH),
    class_mode='categorical')
sample_training_images, _ = next(train_data_gen)


def plotImages(images_arr):
    fig, axes = plt.subplots(1, 3, figsize=(20, 20))
    axes = axes.flatten()
    for img, ax in zip(images_arr, axes):
        ax.imshow(img)
        ax.axis('off')
    plt.tight_layout()
    plt.show()


plotImages(sample_training_images[:3])

# Building a Sequential Model
model = Sequential()

# Convolution blocks
model.add(Conv2D(32, (3, 3), padding='same', input_shape=(IMG_HEIGHT, IMG_WIDTH, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=2))

model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
model.add(MaxPooling2D(pool_size=2))

model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
model.add(MaxPooling2D(pool_size=2))

# Classification layers
model.add(Flatten())

model.add(Dense(64, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(32, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(6, activation='softmax'))

# Defining an optimizer, a loss function, and other useful training parameter
model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['accuracy'])

# Training the model by using the train dataset
model.summary()

image = visualkeras.layered_view(model, legend=True, spacing=20)
plt.imshow(image)
plt.show()

batch_size = 45
epochs = 55

num_cardboard_train = len(os.listdir(imagepath_cardboard_dir))
num_glass_train = len(os.listdir(imagepath_glass_dir))
num_metal_train = len(os.listdir(imagepath_metal_dir))
num_paper_train = len(os.listdir(imagepath_cardboard_dir))
num_plastic_train = len(os.listdir(imagepath_glass_dir))
num_trash_train = len(os.listdir(imagepath_trash_dir))

num_cardboard_test = len(os.listdir(graypath_cardboard))
num_glass_test = len(os.listdir(graypath_glass))
num_metal_test = len(os.listdir(graypath_metal))
num_paper_test = len(os.listdir(graypath_paper))
num_plastic_test = len(os.listdir(graypath_plastic))
num_trash_test = len(os.listdir(graypath_trash))

# Total number of images in training and testing
total_train = (num_cardboard_train + num_glass_train + num_metal_train + num_paper_train + num_plastic_train +
               num_trash_train)
total_test = num_cardboard_test + num_glass_test + num_metal_test + num_paper_test + num_plastic_test + num_trash_test

# Train the model
history = model.fit(
    train_data_gen,
    validation_data=train_data_gen,
    steps_per_epoch=total_train // batch_size,
    epochs=epochs,
    validation_steps=total_test // batch_size,
    callbacks=[tf.keras.callbacks.EarlyStopping(
        monitor='val_loss',
        min_delta=0.01,
        patience=7)]
)
test_loss, test_acc = model.evaluate(test_data_gen)
print('Test accuracy: {} Test Loss: {} '.format(test_acc * 100, test_loss))

train_acc = history.history['accuracy']  # store training accuracy in history
val_acc = history.history['val_accuracy']  # store validation accuracy in history
train_loss = history.history['loss']  # store training loss in history
val_loss = history.history['val_loss']  # store validation loss in history

epochs = range(1, len(train_acc) + 1)

plt.plot(epochs, train_acc, 'g', label='Training accuracy')
plt.plot(epochs, val_acc, 'r', label='Validation accuracy')
plt.title('Training and validation accuracy')
plt.legend()

plt.figure()

plt.plot(epochs, train_loss, 'g', label='Training loss')
plt.plot(epochs, val_loss, 'r', label='Validation loss')
plt.title('Training and validation loss')
plt.legend()

plt.show()
